#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>

/************************************/
/*  exemple de création de fenêtres */
/************************************/
void SDL_QuitterAvecErreur(const char *message)
{
    SDL_Log("ERREUR : %s > %s\n", message, SDL_GetError());
    SDL_Quit();
    exit(EXIT_FAILURE);
}
void translation_deplacement2window(SDL_Window *window_1,SDL_Window *window_2)
{
  int deplace1=0;                   //variable pour le premier deplacement
  int j=0;
  for(int i=1;i<40;i++)
  {
  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", 
             SDL_GetError());                // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  /* Création de la fenêtre de gauche */
  window_1 = SDL_CreateWindow(
      "Fenêtre à gauche",                    // codage en utf8, donc accents possibles
      deplace1, deplace1,                                  // coin haut gauche en haut gauche de l'écran
      30+j, 20+j,                              // largeur = 400, hauteur = 300
      SDL_WINDOW_RESIZABLE);                 // redimensionnable

  if (window_1 == NULL) {
    SDL_Log("Error : SDL window 1 creation - %s\n", 
            SDL_GetError());                 // échec de la création de la fenêtre
    SDL_Quit();                              // On referme la SDL       
    exit(EXIT_FAILURE);
  }
  /* Création de la fenêtre de droite */
  window_2 = SDL_CreateWindow(
      "Fenêtre à droite",                    // codage en utf8, donc accents possibles
      1200-deplace1, deplace1,                                // à droite de la fenêtre de gauche
      30+j, 20+j,                              // largeur = 500, hauteur = 300
      0);

  if (window_2 == NULL) {
    /* L'init de la SDL : OK
       fenêtre 1 :OK
       fenêtre 2 : échec */
    SDL_Log("Error : SDL window 2 creation - %s\n", 
            SDL_GetError());                 // échec de la création de la deuxième fenêtre 
    SDL_DestroyWindow(window_1);             // la première fenétre (qui elle a été créée) doit être détruite
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  //translation window1
  int d1=0;
  for(int c=0;c<20;c++)
 {
   SDL_SetWindowPosition(window_1,deplace1+d1, deplace1+d1);
   SDL_Delay(10);
   d1+=2;
 } 
 //translation window 2
 int d2=0; 
 for(int c=0;c<20;c++)
 {
   SDL_SetWindowPosition(window_2,1100-deplace1+d2, deplace1+d2);
   SDL_Delay(10);
   d2+=2;
 }  
  SDL_Delay(100); 
  deplace1= deplace1+15;
  j++;
  }
}
void DeplaceGrandirWindow(SDL_Window *window_3,SDL_Window *window_4)
{ 
 int deplace2=0;                    // Future fenêtre de droite
  int h=0;
  for(int i=1;i<40;i++)
  {
  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", 
             SDL_GetError());                // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  /* Création de la fenêtre de gauche */
  window_3 = SDL_CreateWindow(
      "Fenêtre à gauche",                    // codage en utf8, donc accents possibles
      deplace2, 550-deplace2,                                  // coin haut gauche en haut gauche de l'écran
      30+h, 20+h,                              // largeur = 400, hauteur = 300
      SDL_WINDOW_RESIZABLE);                 // redimensionnable

  if (window_3 == NULL) {
    SDL_Log("Error : SDL window 1 creation - %s\n", 
            SDL_GetError());                 // échec de la création de la fenêtre
    SDL_Quit();                              // On referme la SDL       
    exit(EXIT_FAILURE);
  }
  /* Création de la fenêtre de droite */
  window_4 = SDL_CreateWindow(
      "Fenêtre à droite",                    // codage en utf8, donc accents possibles
      1200-deplace2, 550-deplace2,                                // à droite de la fenêtre de gauche
      30+h, 20+h,                              // largeur = 500, hauteur = 300
      0);

  if (window_4 == NULL) {
    /* L'init de la SDL : OK
       fenêtre 1 :OK
       fenêtre 2 : échec */
    SDL_Log("Error : SDL window 4 creation - %s\n", 
            SDL_GetError());                 // échec de la création de la deuxième fenêtre 
    SDL_DestroyWindow(window_4);             // la première fenétre (qui elle a été créée) doit être détruite
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  SDL_Delay(50); 
  deplace2= deplace2+15;
  h+=2;
  }
 } 
    

void dessiner_un_rendu(SDL_Window *window, SDL_Renderer *renderer)
{
    //Lancement SDL2
    if(SDL_Init(SDL_INIT_VIDEO) != 0)
        SDL_QuitterAvecErreur("Initialisation a echoué");
    
    //création du fenetre
    window = SDL_CreateWindow("fenêtre avec un dessin",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,800,600,0);
    
    if (window == NULL)
        SDL_QuitterAvecErreur("Création du fenetre a echouée");
    
    /*--------------------------------------------------------------------------*/
    
    //création rendu
    renderer = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    
    if (renderer == NULL)
        SDL_QuitterAvecErreur("Création rendu a echouée");
  for(int i=0;i<20;i++)
   { 
    for(int j=0;j<20;j++)
   { 
    SDL_Rect rectangle;
    
    
    SDL_SetRenderDrawColor(renderer,                                                

                         50, 0, 0,                                  // mode Red, Green, Blue (tous dans 0..255)

                         255);                                      // 0 = transparent ; 255 = opaque

    
    
    rectangle.x = 500+5*i;                                                  // x haut gauche du rectangle

    rectangle.y = 200+5*i-20*j;                                                  // y haut gauche du rectangle

    rectangle.w = 40;                                                // sa largeur (w = width)

    rectangle.h = 20;                                                // sa hauteur (h = height)
    
    
    SDL_RenderFillRect(renderer, &rectangle);                         

    
    SDL_SetRenderDrawColor(renderer, 150, 30, 230, 255);
    
    SDL_SetRenderDrawColor(renderer,                                                

                         50, 0, 0,                                  // mode Red, Green, Blue (tous dans 0..255)

                         255);                                      // 0 = transparent ; 255 = opaque
    SDL_Rect rectangle2;
    
    
    SDL_SetRenderDrawColor(renderer,                                                

                         50, 0, 0,                                  // mode Red, Green, Blue (tous dans 0..255)

                         255);                                      // 0 = transparent ; 255 = opaque

    
    
    
    rectangle2.x = 500+5*i-10*j;                                                  // x haut gauche du rectangle

    rectangle2.y = 200+5*i+7*j;                                                  // y haut gauche du rectangle

    rectangle2.w = 40;                                                // sa largeur (w = width)

    rectangle2.h = 20;                                                // sa hauteur (h = height)
    
    
    SDL_RenderFillRect(renderer, &rectangle2);                         

    
    SDL_SetRenderDrawColor(renderer, 150, 30, 230, 255);
    /*SDL_RenderDrawLine(renderer,                                      

                     500, 200,                                          // x,y du point de la première extrémité

                     40, 20);                                     // x,y seconde extrémité
    
     
   
    */
    
    SDL_RenderPresent(renderer);                         // affichage
   
    SDL_Delay(20);
    SDL_RenderClear(renderer);
   } 
   } 
     
    /*--------------------------------------------------------------------------*/
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}


