#include "mon_code.h"
#include <SDL2/SDL.h>
#include <stdio.h>
int main(int argc, char **argv) {
  (void)argc;
  (void)argv;


  SDL_Window 
 

       *window_1 = NULL,                     // Future fenêtre 1
       *window_2 = NULL,                     // Future fenêtre 2
       *window_3 = NULL,                     // Future fenêtre 3
       *window_4 = NULL,                    // Future fenêtre 4
       *window_5 = NULL;                     // Future fenêtre 5

  SDL_Renderer *renderer=NULL;
  translation_deplacement2window(window_1,window_2);
  
  DeplaceGrandirWindow(window_3,window_4);
  
  dessiner_un_rendu(window_5,renderer);   
    
  /* Normalement, on devrait ici remplir les fenêtres... */
  SDL_Delay(7000);                           // Pause exprimée  en ms donc 7s ici
  /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
  SDL_DestroyWindow(window_4);               // la fenêtre 4  
  SDL_DestroyWindow(window_3);               // la fenêtre 3
   
  /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
  SDL_DestroyWindow(window_2);               // la fenêtre 2  
  SDL_DestroyWindow(window_1);               // la fenêtre 1
  SDL_DestroyWindow(window_5);               // la fenêtre 5
    
  
  SDL_Quit();                                // la SDL

  return 0;
}
